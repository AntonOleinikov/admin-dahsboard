import types from '../actions/types';

const initialState = {
  quotesData: {
    rows: [],
  },
  authorsList: []
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_QUOTES_LIST_SUCCESS:
      console.log('Reducer', );
      return {
        ...state,
        quotesData: {
          rows: [...state.quotesData.rows, ...action.payload.rows],
          count: action.payload.count
        }
      };
    case types.UPDATE_QUOTE_SUCCESS:
      return {...state, quotesList: action.payload};
    case types.DELETE_QUOTE_SUCCESS:
      return {...state, quotesList: action.payload};
    case types.GET_AUTHORS_LIST_SUCCESS:
      return {...state, authorsList: action.payload};
    default:
      return state
  }
};

export default appReducer

import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import actions from '../actions';

const EditQuote = (props) => {
  let [id, setId] = useState(null);
  let [text, setText] = useState('');
  let [author_id, setAuthor] = useState('');

  useEffect(() => {
    const { state } = props.history.location;
    props.getAuthorsList();

    setId(state.id);
    setText(state.text);
    setAuthor(state.author_id);
  }, []);

  const handleSubmit = () => {
    const { history, updateOneQuote } = props;
    if(!validateForm()) {
      return;
    }

    updateOneQuote({
      id,
      body: {
        text,
        author_id
      }
    });

    history.push('/quotes-list');
  };


  const handleCancel = () => {
    props.history.push('/quotes-list');
  };

  const validateForm = () => {
    const {
      history: {
        location: {
          state
        }
      }
    } = props;

    return (text && author_id) || (state.text && state.author_id);
  };

    const {
      history: {
        location: { state }
      }
    } = props;


  const selectedAuthor = props.authorsList.find(e => e.id === author_id);
  const authorsListElement = props.authorsList && (
    <select value={author_id}
            defaultValue={selectedAuthor}
            className={'authors-select'}
            onChange={(e) => setAuthor(e.target.value)}>
      {props.authorsList.map(author => (
        <option value={author.id} >{author.full_name}</option>
      ))}
    </select>
  );

    return (
      <div className={'form-container'}>
        {authorsListElement}
        <textarea
          className={"textarea-field"}
          value={text}
          onChange={(e) => setText(e.target.value)}
          placeholder="Quote text"
        />
        <div className={"form-controls-container"}>
          <button onClick={handleSubmit} disabled={!validateForm()}>Save</button>
          <button className={"clear-button"} onClick={handleCancel}>Cancel</button>
        </div>
      </div>
    );
  };

const mapStateToProps = state => {
  return {
    authorsList: state.authorsList
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(EditQuote);

import React, {useState, useRef} from 'react';
import {connect} from 'react-redux';
import actions from '../actions';

const AddQuote = (props) => {
  let [fullName, setFullName] = useState(null);
  let fileRef = useRef();

  const handleSubmit = () => {
    const {history, createOneAuthor} = props;
    const formData = new FormData();

    formData.set('full_name', fullName);
    formData.append('image', fileRef.current.files[0]);

    // createOneAuthor({
    //   body: {
    //     image: formData,
    //     full_name: fullName
    //   }
    // });

    createOneAuthor({
      formData
    });
    history.push('/authors-list');
  };

  const handleClear = () => {
    setFullName(null);
    fileRef = null;
  };

  const validateForm = () => {
    return fullName && fileRef.current && fileRef.current.files;
  };

  return (
    <div className={'form-container'}>
      <input onChange={(e) => setFullName(e.target.value)}
             value={fullName}
             type="text"
             placeholder={'Full name'}/>
      <input ref={fileRef}
             type="file"/>
      <div className={"form-controls-container"}>
        <button onClick={handleSubmit} disabled={!validateForm()}>Create</button>
        <button className={"clear-button"} onClick={handleClear}>Clear</button>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    authorsList: state.authorsList
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(AddQuote);

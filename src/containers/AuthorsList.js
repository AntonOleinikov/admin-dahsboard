import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import uuidv4 from 'uuid/v4';
import actions from '../actions';

const AuthorsList = (props) => {

  useEffect(() => {
    props.getAuthorsList();
  }, []);

  // componentDidMount() {
  //   window.addEventListener('scroll', this.onScroll, false);
  // }
  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.onScroll, false);
  // }

  const handleEdit = (quote) => {
    props.history.push(`/edit-author/${quote.id}`, quote);
  };

  const handleDelete = (quote) => {
    props.deleteOneQuote({
      id: quote.id
    });
  };

  const quotesListMap = props.authorsList && props.authorsList.length ? props.authorsList.map(e => (
    <div key={uuidv4()} className={"authors-item-container"}>
      <img className={"authors-item-image"} src={e.image} alt=""/>
      <h4 className={"authors-item-title"}>{e.full_name}</h4>
      <div className="authors-item-controls">
        <button onClick={() => handleEdit(e)} className={"quote-item-edit-btn"}>Edit</button>
        <button onClick={() => handleDelete(e)} className={"quote-item-delete-btn"}>Delete</button>
      </div>
    </div>
  )) : null;

  return (
    <div>
      <h1>Authors List</h1>
      <div className={'authors-list-container'}>
        {quotesListMap}
      </div>
    </div>
  )
};

const mapStateToProps = state => {
  return {
    authorsList: state.authorsList
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AuthorsList));

import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import uuidv4 from 'uuid/v4';
import actions from '../actions';
import store from '../store';

const QuotesList = (props) => {
  let [stopLoading, setStopLoading] = useState(false);
  useEffect(() => {
    props.getQuotesList({ offset: 0 });
    // window.addEventListener('scroll', (e) => {
    //   onScroll(e, props.quotesList)
    // });
    //
    // return () => {
    //   window.removeEventListener('scroll', (e) => onScroll(e, props.quotesList));
    // }

  }, []);

  useEffect(() => {
    window.addEventListener('scroll', onScroll);

    return () => {
      window.removeEventListener('scroll', onScroll);
    }
  }, [props.quotesList]);

  const onScroll = () => {
    if (
      (window.innerHeight + window.scrollY) === document.body.offsetHeight
    ) {
      updateQuotesList();
    }
  };

  const updateQuotesList = () => {
    const offset = props.quotesList.length;

    if (!stopLoading) {
      props.getQuotesList({ offset });
    }
    if(props.quotesCount === offset) {
      setStopLoading(true);
    }
  };

  const handleEdit = (quote) => {
    // props.history.push(`/edit-quote/${quote.id}`, quote);
  };

  const handleDelete = (quote) => {
    props.deleteOneQuote({
      id: quote.id
    });
  };

  const quotesListMap = props.quotesList && props.quotesList.length ? props.quotesList.map(e => (
    <div key={uuidv4()} className={"quote-item-container"}>
      <img className={"quote-item-image"} src={e.Author.image} alt=""/>
      <div className={"quote-item-text"}>
        <h4 className={"quote-item-title"}>{e.Author.full_name}</h4>
        <div>{e.text}</div>
        <div className="quote-item-controls">
          <button onClick={() => handleEdit(e)} className={"quote-item-edit-btn"}>Edit</button>
          <button onClick={() => handleDelete(e)} className={"quote-item-delete-btn"}>Delete</button>
        </div>
      </div>
    </div>
  )) : null;
  return (
    <div className={'quote-list-container'}>
      <h1>Quotes List page</h1>
      {quotesListMap}
    </div>
  )
};

const mapStateToProps = state => {
  console.log('quotesList', state.quotesList);
  return {
    quotesList: state.quotesData.rows,
    quotesCount: state.quotesData.count
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(QuotesList));

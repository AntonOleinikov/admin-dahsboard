import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import actions from '../actions';

const AddQuote = (props) => {
  let [text, setText] = useState('');
  let [author_full_name, setAuthor] = useState('');

  useEffect(() => {
    props.getAuthorsList();
  }, []);

  const handleSubmit = () => {
    const {history, createOneQuote} = props;
    if (!validateForm()) {
      return;
    }
    createOneQuote({
      body: {
        author_full_name,
        text
      }
    });

    history.push('/quotes-list');
  };

  const handleClear = () => {
    setText('');
    setAuthor('');
  };

  const validateForm = () => {
    return text && author_full_name;
  };

  const authorsListElement = props.authorsList && (
    <select value={author_full_name}
            className={'authors-select'}
            onChange={(e) => setAuthor(e.target.value)}>
      {props.authorsList.map(author => (
        <option value={author.full_name}>{author.full_name}</option>
      ))}
    </select>
  );

  return (
    <div className={'form-container'}>
      {authorsListElement}
      <textarea
        className={"textarea-field"}
        onChange={(e) => setText(e.target.value)}
        placeholder="Quote text"
      />
      <div className={"form-controls-container"}>
        <button onClick={handleSubmit} disabled={!validateForm()}>Create</button>
        <button className={"clear-button"} onClick={handleClear}>Clear</button>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  console.log('quotes list', state.authorsList);
  return {
    authorsList: state.authorsList
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(AddQuote);

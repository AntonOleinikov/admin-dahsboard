import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import AddQuoteForm from './containers/AddQuote';
import QuotesList from './containers/QuotesList';
import AuthorsList from './containers/AuthorsList';
import AddAuthor from './containers/AddAuthor';
import EditQuote from './containers/EditQuote';
import './App.css';

function App() {
  return (
    <div className="app">
      <Header />
      <div className="container">
        <Switch>
          <Route exact path='/create-quote' component={AddQuoteForm} />
          <Route path='/edit-quote/:id' component={EditQuote} />
          <Route path='/quotes-list' component={QuotesList} />
          <Route path='/authors-list' component={AuthorsList} />
          <Route path='/create-author' component={AddAuthor} />
        </Switch>
      </div>
    </div>
  );
}

export default App;

import axios from 'axios';
import types from './types';
const URL = 'http://localhost:3000';
export default {
  getQuotesList: ({ offset }) => dispatch => {
      dispatch({
        type: types.GET_QUOTES_LIST_LOADING,
        payload: []
      });

      axios.get(`${URL}/quotes/${offset}`)
        .then((response) => dispatch({
          type: types.GET_QUOTES_LIST_SUCCESS,
          payload: response.data
        })).catch((response) => dispatch({
        type: types.GET_QUOTES_LIST_ERROR,
        payload: response.error
      }))
    },
  createOneQuote: ({ body }) => dispatch => {
    dispatch({
      type: types.CREATE_QUOTE_LOADING,
      payload: []
    });

    axios.post(`${URL}/quotes`, body)
      .then((response) => dispatch({
        type: types.CREATE_QUOTE_SUCCESS,
        payload: response.data
      }))
      .catch((response) => dispatch({
      type: types.CREATE_QUOTE_ERROR,
      payload: response.error
    }))
  },

  updateOneQuote: ({ body, id }) => (dispatch, getState) => {
    const updatedList = getState().quotesList.map(quote =>
      quote.id === id ? Object.assign({}, quote, body) : quote);

    dispatch({
      type: types.UPDATE_QUOTE_LOADING,
      payload: []
    });

    axios.put(`${URL}/quotes/${id}`, body)
      .then((_) => dispatch({
        type: types.UPDATE_QUOTE_SUCCESS,
        payload: updatedList
      }))
      .catch((response) => dispatch({
        type: types.UPDATE_QUOTE_ERROR,
        payload: response.error
      }))
  },

  deleteOneQuote: ({ id }) => (dispatch, getState) => {
    dispatch({
      type: types.DELETE_QUOTE_LOADING,
      payload: []
    });

    axios.delete(`${URL}/quotes/${id}`)
      .then(() => {
        const { quotesList } = getState();
        const updatedQuotesList = quotesList.filter(e => e.id !== id);

        dispatch({
          type: types.DELETE_QUOTE_SUCCESS,
          payload: updatedQuotesList
        })
      })
      .catch((response) => dispatch({
        type: types.DELETE_QUOTE_ERROR,
        payload: response.error
      }))
  },

  getAuthorsList: () => (dispatch, getState) => {
    dispatch({
      type: types.GET_AUTHORS_LIST_LOADING,
      payload: []
    });

    axios.get(`${URL}/authors`)
      .then((response) => {
        dispatch({
          type: types.GET_AUTHORS_LIST_SUCCESS,
          payload: response.data
        })
      })
      .catch((response) => dispatch({
        type: types.GET_AUTHORS_LIST_ERROR,
        payload: response.error
      }))
  },

  createOneAuthor: ({ formData }) => dispatch => {
    dispatch({
      type: types.CREATE_AUTHOR_LOADING,
      payload: []
    });

    axios.post(`${URL}/authors`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' }
    })
      .then((response) => {
        dispatch({
          type: types.CREATE_AUTHOR_SUCCESS,
          payload: response.data
        })
      })
      .catch((response) => dispatch({
        type: types.CREATE_AUTHOR_ERROR,
        payload: response.error
      }))
  }
}

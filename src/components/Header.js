import React from 'react';
import {Link} from 'react-router-dom';

const Header = (props) => {

  return (
    <header className="app-header">
      <span className="logo">Quotes API Dashboard</span>
      <div className="nav-menu">
        <Link to={'/create-quote'} className="nav-link">Create quote</Link>
        <Link to={'/quotes-list'} className="nav-link">Quotes list</Link>
        <Link to={'/authors-list'} className="nav-link">Authors list</Link>
        <Link to={'/create-author'} className="nav-link">Create Author</Link>
      </div>
    </header>
  )
};

export default Header;
